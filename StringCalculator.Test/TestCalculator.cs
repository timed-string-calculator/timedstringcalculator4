﻿
using NUnit.Framework;
using System;

namespace StringCalculator.Test
{
    [TestFixture]
    public class TestCalculator
    {
        private Calculator _calculator = null;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumber_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 3;
            var actual = _calculator.Add("1,2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumber_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("1,2,6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewlineAsDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("1,2\n6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharcaterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("//;\n1;2;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumbers_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative not allowed -6";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;2;-6"));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumbersMoreThanAThousand_WHEN_Adding_THEN_IngoreThem()
        {
            var expected = 3;
            var actual = _calculator.Add("//;\n1;2;6000");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharcaterCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("//;;\n1;;2;;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharcaterMultipleCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("//[;][*]\n1;2*6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharcaterMultipleCustomDelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 9;
            var actual = _calculator.Add("//[;;][**]\n1;;2**6");

            Assert.AreEqual(expected, actual);
        }
    }
}
