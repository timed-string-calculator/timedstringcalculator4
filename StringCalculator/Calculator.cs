﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class Calculator
    {
        private readonly string customDelimiterId = "//";
        private readonly string newline = "\n";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string numbersSection = GetNumbersSection(numbers);
            string[] delimiters = GetDelimiters(numbers);
            List<int> numbersList = GetNumbers(delimiters, numbersSection);
            ValidateNumbers(numbersList);

            return GetSum(numbersList);
        }

        private void ValidateNumbers(List<int> numbersList)
        {
            List<string> negativenumbers = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativenumbers.Add(number.ToString());
                }
            }

            if (negativenumbers.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(",", negativenumbers)}");
            }
        }

        private string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimitersId = $"{customDelimiterId}[";
            var multipleCustomDelimitersSperator = $"]{newline}";
            var multipleCustomDelimitersSplitter = "][";


            if (numbers.StartsWith(multipleCustomDelimitersId))
            {
                string delimiter = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomDelimitersSperator) - (multipleCustomDelimitersSperator.Length + 1));

                return delimiter.Split(new[] { multipleCustomDelimitersSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(customDelimiterId) + customDelimiterId.Length, numbers.IndexOf(newline) - (newline.Length + 1)) };
            }

            return new[] { ",", newline };
        }

        private string GetNumbersSection(string numbers)
        {
            if (numbers.StartsWith(customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf(newline) + 1);
            }

            return numbers;
        }

        private int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        private List<int> GetNumbers(string[] delimiters, string numbers)
        {
            string[] numbersArray = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }
    }
}
